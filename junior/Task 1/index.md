# Задание 1 #
Первом задание это краткое описание языка и задача на создание sqlite БД.

Проверка задачи будет выполнятся через PR (Pull-Request).

## Введение ##

 - [http://php.net](http://php.net)
 - [http://getjump.me/ru-php-the-right-way/](http://getjump.me/ru-php-the-right-way/)

## Описание задачи ##

Необходимо создать БД в sqlite. Для создания БД можно использовать PHINX http://docs.phinx.org/en/latest/

Структура БД:

```
products (товары)
    id (первичный ключ) 
    name (название товара)
    description (описание товара)
    image (картинка)
    price (цена)
    date_create (дата создания)
    date_update (дата обновления)
    deleted (флаг удаления)

order (заказ)
    id (первичный ключ) 
    date_create(дата создания)
    date_update(дата обновления)
    user_id (пользователь)
    sum (сумма покупки)
    address (адрес доставки)
    payment_status (статус оплаты)

order_products (товары в корзине)
    order_id (id заказа)
    product_id (id товара)
    price (цена)

users (авторизация пользователей)
    id (первичный ключ) 
    name (имя)
    login (логин)
    salt (соль)
    hash (хэш пароля)
    date_create(дата создания)
    date_update(дата обновления)
    deleted (флаг удаления)
```

Для установки phinx использовать composer.

### Полезные ссылки: ###

 - [https://habrahabr.ru/post/145946/](https://habrahabr.ru/post/145946/)
 - [http://docs.phinx.org/en/latest/index.html](http://docs.phinx.org/en/latest/index.html)
