# Занятие 1 #

Вводное занятие посвященное переменным, константам языка. Также рассматриваются операторы и управляющие конструкции.

## Переменные ##
Прочитать http://php.net/manual/ru/language.variables.basics.php

* Задать несколько переменных с именами соответсвующие правилам PHP, 
выполнить арифметические операции: сложение, деление и умножение, вывести результаты в консоль.

* Задать несколько переменных с именами не соответствующим правилам PHP

Прочитать http://php.net/manual/ru/language.variables.predefined.php

* Попробовать вывести в консоль несколько предопределенных переменных

Прочитать http://php.net/manual/ru/language.variables.scope.php

* Попробовать примеры из данной статьи.

## Константы ##

Прочитать и выполнить примеры из статьи http://php.net/manual/ru/language.constants.syntax.php
* Выполнить примеры https://www.w3resource.com/php-exercises/php-basic-exercises.php

## Операторы и управляющие конструкции ##
Прочитать http://php.net/manual/ru/language.operators.php , http://php.net/manual/ru/language.control-structures.php

* Вывести числа Фибоначчи разделенные пробелом в консоль, кол-во чисел будет задаваться константой
* Решить задачи https://www.w3resource.com/php-exercises/php-array-exercises.php , https://www.w3resource.com/php-exercises/php-for-loop-exercises.php

