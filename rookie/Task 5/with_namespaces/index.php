<?php
require('init.php');
?>
<html>
<meta charset="UTF-8">
<head>
    <?php
        \Helpers\Header::output();
    ?>
</head>
<body>
<header>
    <h1>Используем autoload и namespaces</h1>
</header>
<div class="main">
    <img src="images/The_Rookie.jpg" alt="" style="float:left; margin-right: 10px;">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lectus orci, mollis vitae ultricies vel, rutrum
        vitae ipsum. Vestibulum varius sapien tortor, sed cursus odio rhoncus sit amet. Phasellus finibus nibh neque, eu
        varius est congue vitae. In mauris dui, vehicula ac elit vel, luctus auctor elit. Morbi in rutrum ante. Donec
        sem odio, semper ac lacinia sit amet, porta sit amet metus. Duis nibh sapien, vulputate id tristique sit amet,
        varius sed tellus.</p>

    <p>Phasellus eu lorem in leo tincidunt venenatis. Fusce et luctus nisi. Pellentesque et finibus neque, sed
        scelerisque dolor. Sed a est magna. Donec nec erat quam. Donec aliquet, nisl ut malesuada luctus, magna arcu
        volutpat magna, in fermentum erat sem quis sem. Mauris orci eros, elementum ut bibendum et, consequat et sem.
        Donec finibus ante sed ullamcorper vehicula. Class aptent taciti sociosqu ad litora torquent per conubia nostra,
        per inceptos himenaeos. Pellentesque convallis, magna eu auctor suscipit, nisi odio pharetra mi, eu lobortis
        nunc tellus sed mi. Sed ut justo non arcu hendrerit posuere. Aenean eget orci sem. Suspendisse volutpat pretium
        tellus.</p>

    <p>Donec eget hendrerit nisl. Aliquam lobortis eleifend tellus quis ultricies. Morbi consectetur nisi libero, quis
        laoreet eros venenatis eu. Aenean in tempus orci, vitae elementum lacus. Nam pellentesque metus justo, eget
        hendrerit libero tempus non. Nunc porttitor auctor justo. Sed lobortis, libero in eleifend fringilla, nulla
        risus mollis velit, ac eleifend magna lectus vitae sem.</p>

    <p>Nunc rutrum convallis lacus, ut dictum nibh sollicitudin a. Morbi feugiat volutpat interdum. Phasellus quis
        tempor lacus. Nulla id efficitur nibh. Donec vitae nulla nisi. Nullam quis accumsan justo. Etiam velit ex,
        feugiat vel mattis euismod, aliquam ac lacus. Proin id feugiat lacus. Ut consectetur ante in volutpat mollis.
        Suspendisse nec dolor eu risus viverra bibendum sed eget ipsum. Nulla facilisi. Curabitur purus arcu, viverra a
        egestas in, congue et augue. Nam aliquam sapien id orci imperdiet, vitae rutrum enim ultricies. Aenean ornare
        non elit sed feugiat.</p>
</div>
<footer>
    <?php \Helpers\Footer::output(); ?>
</footer>
</body>
</html>
